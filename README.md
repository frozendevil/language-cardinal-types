This is a table of pluralization-related information for every langauge described in the [Unicode CLDR 'Plural Rules' chart](http://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html).

Specifically the information provided is the information needed to correctly populate a [`.stringsdict` file](https://developer.apple.com/library/ios/documentation/MacOSX/Conceptual/BPInternational/StringsdictFileFormat/StringsdictFileFormat.html)


Format:

- `name`: The name of the language (in English)
- `code`: The ISO 639‑1 language code
- `traits`: An array that will contain at least "other", but may also include any combination of "zero", "one", "two", "few", "many"